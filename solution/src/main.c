#include <stdio.h>
#include <sysexits.h>

#include "bmp.h"
#include "file.h"
#include "image.h"
#include "rotate-transformation.h"

static const char* const err_msgs[] = {
        [EX_USAGE] = "The program requires 2 arguments: image-transformer <input file> <output file>\n",
        [EX_NOINPUT] = "Can't open file\n",
        [EX_DATAERR] = "Invalid bmp format\n",
        [EX_IOERR] = "Error while trying to close the file\n",
        [EX_CANTCREAT] = "Couldn't creat file or access is denied\n",
};

int main( int argc, char** argv ) {
    if (argc != 3) {
        fprintf(stderr, "%s", err_msgs[EX_USAGE]);
        return EX_USAGE;
    }
    FILE* in = NULL;
    if (!file_open(argv[1], &in, "rb")){
        fprintf(stderr,  "%s", err_msgs[EX_NOINPUT]);
        return EX_NOINPUT;
    }
    struct image img = {0};
    switch (from_bmp(in, &img)) {
        case READ_INVALID_BITS:
        case READ_INVALID_SIGNATURE:
        case READ_INVALID_HEADER:
            fprintf(stderr,  "%s", err_msgs[EX_DATAERR]);
            file_close(in);
            return EX_DATAERR;
        case READ_FROM_NULL_FILE:
            fprintf(stderr,  "%s", err_msgs[EX_NOINPUT]);
            file_close(in);
            return EX_NOINPUT;
        case READ_OK:
            break;
    }
    if(!file_close(in)) {
        fprintf(stderr,  "%s", err_msgs[EX_IOERR]);
        image_destroy(&img);
        return EX_IOERR;
    }
    struct image new_img = rotate(&img);
    image_destroy(&img);
    FILE* out = NULL;
    if(!file_open(argv[2], &out, "wb")) {
        fprintf(stderr,  "%s", err_msgs[EX_CANTCREAT]);
        image_destroy(&new_img);
        return EX_CANTCREAT;
    }
    if (to_bmp(out, &new_img)) {
        fprintf(stderr,  "%s", err_msgs[EX_IOERR]);
        image_destroy(&new_img);
        file_close(out);
        return EX_IOERR;
    }
    image_destroy(&new_img);
    if (!file_close(out)) {
        fprintf(stderr,  "%s", err_msgs[EX_IOERR]);
        return EX_IOERR;
    }
    return EX_OK;
}
